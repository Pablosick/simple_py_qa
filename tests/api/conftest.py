import pytest

from src.client_class import ApiClient
from conf.configuration import STORE_URL


@pytest.fixture(scope="function")
def api_client():
    return ApiClient(STORE_URL)
