import json

from src.enums.global_enums import GlobalErrorMessages


def test_create_new_order(api_client):
    new_order = {
        "id": 5,
        "petId": 6,
        "quantity": 7,
        "shipDate": "2022-01-01T12:00:00.000+0000",
        "status": "placed",
        "complete": False}
    response = api_client.post("/store/order", json=new_order)
    assert response.status_code == 200, GlobalErrorMessages.WRONG_STATUS_CODE.value

    # Verification of sent data
    order_id = json.loads(response.body)["id"]
    response = api_client.get(f"/store/order/{order_id}")
    assert response.status_code == 200, GlobalErrorMessages.WRONG_STATUS_CODE.value


    # Verify the order details
    for key in new_order.keys():
        assert json.loads(response.body)[key] == new_order[key]

