from src.enums.global_enums import GlobalErrorMessages


def test_get_inventory(api_client):
    response = api_client.get("store/inventory")
    assert response.status_code == 200, GlobalErrorMessages.WRONG_STATUS_CODE.value
