from pydantic import BaseModel


class Order(BaseModel):
    id: int
    petId: int
    quantity: int
    shipDate: str