import requests

from src.response_class import ApiResponse


class ApiClient:

    def __init__(self, base_url):
        self.base_url = base_url

    def get(self, endpoint, params=None, headers=None):
        url = self.base_url + endpoint
        response = requests.get(url, params=params, headers=headers)
        return ApiResponse(response.status_code, response.headers, response.text)

    def post(self, endpoint, json=None, headers=None):
        url = self.base_url + endpoint
        response = requests.post(url, json=json, headers=headers)
        return ApiResponse(response.status_code, response.headers, response.text)

    def put(self, endpoint, data=None, headers=None):
        url = self.base_url + endpoint
        response = requests.put(url, params=data, headers=headers)
        return ApiResponse(response.status_code, response.headers, response.text)

    def delete(self, endpoint, params=None, headers=None):
        url = self.base_url + endpoint
        response = requests.delete(url, params=params, headers=headers)
        return ApiResponse(response.status_code, response.headers, response.text)
